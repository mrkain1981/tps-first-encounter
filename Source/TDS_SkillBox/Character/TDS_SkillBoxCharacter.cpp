// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS_SkillBoxCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATDS_SkillBoxCharacter::ATDS_SkillBoxCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'")); 
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDS_SkillBoxCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDS_SkillBoxCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_SkillBoxCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_SkillBoxCharacter::InputAxisY);
	PlayerInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATDS_SkillBoxCharacter::InputWheel);
	PlayerInputComponent->BindAction(TEXT("AltPressed"), IE_Pressed, this, &ATDS_SkillBoxCharacter::LeftAltPressed);
	PlayerInputComponent->BindAction(TEXT("AltPressed"), IE_Released, this, &ATDS_SkillBoxCharacter::LeftAltReleased);
	PlayerInputComponent->BindAction(TEXT("ShiftPressed"), IE_Pressed, this, &ATDS_SkillBoxCharacter::ShiftPressed);
	PlayerInputComponent->BindAction(TEXT("ShiftPressed"), IE_Released, this, &ATDS_SkillBoxCharacter::ShiftReleased);

}

void ATDS_SkillBoxCharacter::InputAxisY(float Value)
{
	
    AxisY = Value;
    SideMove = (AxisY != 0);

	if (SideMove && MovementState == EMovementState::Sprint_State)
	{
		CharacterUpdate();
	}
}

void ATDS_SkillBoxCharacter::InputAxisX(float Value)
{
	AxisX = Value;
	BackwardMove = AxisX < 0;
	if (BackwardMove && MovementState == EMovementState::Sprint_State)
	{
		CharacterUpdate();
	}
}

void ATDS_SkillBoxCharacter::InputWheel(float Value)
{
	if (Value != 0.0f)
	{
		float NewHeight = CameraBoom->TargetArmLength - CamHeightStep * Value;

		if (NewHeight > MinCamHeight && NewHeight < MaxCamHeight)
		{
			CameraBoom->TargetArmLength = NewHeight;
		}
	}
}

void ATDS_SkillBoxCharacter::MovementTick(float DeltaTime)
{
    AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult ResultHit;


		//float ViewportSizeX;
		//float ViewportSizeY;

		//myController->GetMousePosition(ViewportSizeX, ViewportSizeY);

		//const FVector2D Position(ViewportSizeX, ViewportSizeY);
		//myController->GetHitResultAtScreenPosition( Position, 
		//	ECollisionChannel::ECC_MAX, 
		//	false, ResultHit);
		//UE_LOG( LogTemp, Warning, TEXT("Location: %s"), *ResultHit.Location.ToString()//
		//);
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);

		FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
	}
}

void ATDS_SkillBoxCharacter::LeftAltPressed()
{
	if (MovementState != EMovementState::Walk_State)
	{
		PrevMovementState = MovementState;
	}
	if (MovementState != EMovementState::Sprint_State)
	{
		ChangeMovementState(EMovementState::Walk_State);
	}
}

void ATDS_SkillBoxCharacter::LeftAltReleased()
{
	if (MovementState != EMovementState::Sprint_State)
	{
		MovementState = PrevMovementState;
		CharacterUpdate();
	}
}

void ATDS_SkillBoxCharacter::ShiftPressed()
{
	if (!SideMove && !BackwardMove)
	{
		MovementState = PrevMovementState;
		ChangeMovementState(EMovementState::Sprint_State);
	}
	else
	{
		//PrevMovementState = MovementState;
	}
}

void ATDS_SkillBoxCharacter::ShiftReleased()
{
	MovementState = PrevMovementState;
	CharacterUpdate();
}

void ATDS_SkillBoxCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState) 
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Sprint_State:
		if (!BackwardMove && !SideMove)
		{
			ResSpeed = MovementInfo.SprintSpeed;
		}
		else
		{
			// ������ �� ������ ����, �� �� ������, ����� �� �����������
			if (PrevMovementState == EMovementState::Sprint_State)
			{
				PrevMovementState = EMovementState::Run_State;
			}
			MovementState = PrevMovementState;
			CharacterUpdate();
		}
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDS_SkillBoxCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	if (MovementState != EMovementState::Walk_State) // ���� �������� ������ - ������� ��� ���������� ��� �� ������ ��������
	{
		MovementState = NewMovementState;
		CharacterUpdate();
	} 
	else
	{
		PrevMovementState = NewMovementState; // �� ��������� ��������� ����-������������ �����, ����� ��������� ���������� ���������� Left Alt
	}
}


