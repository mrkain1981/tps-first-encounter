// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS_SkillBoxGameMode.h"
#include "TDS_SkillBoxPlayerController.h"
#include "../Character/TDS_SkillBoxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_SkillBoxGameMode::ATDS_SkillBoxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_SkillBoxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character")); 
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}