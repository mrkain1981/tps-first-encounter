// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLibrary/Types.h"
#include "TDS_SkillBoxCharacter.generated.h"

UCLASS(Blueprintable)
class ATDS_SkillBoxCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDS_SkillBoxCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent);

	UFUNCTION()
	void InputAxisY(float Value);

	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
		void InputWheel(float Value);

	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION()
		void LeftAltPressed();
	UFUNCTION()
		void LeftAltReleased();

	UFUNCTION()
		void ShiftPressed();
	UFUNCTION()
		void ShiftReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float FindRotatorResultYaw;
	bool CanSprint = false;

	UPROPERTY(BlueprintReadWrite)
	bool SideMove = false;

	UPROPERTY(BlueprintReadWrite)
	bool BackwardMove = false;


	UPROPERTY(BlueprintReadWrite)
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY()
		FCharacterSpeed MovementInfo;

	UPROPERTY(BlueprintReadWrite)
		float CamHeightStep = 30.0f;
	UPROPERTY(BlueprintReadWrite)
		float MinCamHeight = 600.0f;
	UPROPERTY(BlueprintReadWrite)
		float MaxCamHeight = 1400.0f;


	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:

	EMovementState PrevMovementState = EMovementState::Run_State;

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
};

