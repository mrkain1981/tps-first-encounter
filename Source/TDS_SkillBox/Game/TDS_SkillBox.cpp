// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS_SkillBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_SkillBox, "TDS_SkillBox" );

DEFINE_LOG_CATEGORY(LogTDS_SkillBox)
 